$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({interval:6000})
});

  $('#exampleModal').on('show.bs.modal', function(e){
    console.log('Modal show up!');
    $('#contactoBtn').removeClass('btn-primary');
    $('#contactoBtn').addClass('btn-outline-success');
    $('#contactoBtn').prop('disabled', true);
  });

  $('#exampleModal').on('shown.bs.modal', function(e){
    console.log('Modal is showed!');
    
  });

  $('#exampleModal').on('hide.bs.modal', function(e){
    console.log('Modal is hide!');
    
  });

  $('#exampleModal').on('hidden.bs.modal', function(e){
    console.log('Modal was hidden!');
    $('#contactoBtn').removeClass('btn-outline-success');
    $('#contactoBtn').addClass('btn-primary');
    $('#contactoBtn').prop('disabled', false);
  });